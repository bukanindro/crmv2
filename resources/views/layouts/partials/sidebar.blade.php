<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('/home') }}" class="brand-link logo-switch"> 
        <span class="brand-text logo-xs font-weight-bold text-uppercase">CRM</span>
        <span class="brand-text font-weight-bold text-uppercase">CRMKinaryatama</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-database"></i>
                    <p>FILE<i class="right fas fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="{{url('/datadasar')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i><p>Data Dasar</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('/karyawan')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i><p>Karyawan</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{url('/instansi')}}" class="nav-link">
                            <i class="far fa-circle nav-icon"></i><p>Instansi</p>
                        </a>
                    </li>
                </ul>
            </li>  
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tasks"></i>
                    <p>PROYEK<i class="right fas fa-angle-left"></i></p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="far fa-circle nav-icon"></i><p>Kunjungan</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="far fa-circle nav-icon"></i><p>Kontak</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="far fa-circle nav-icon"></i><p>Dokumen</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-map"></i>
                    <p>ABSENSI</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-calendar"></i>
                    <p>JADWAL</p>
                </a>
            </li> 
            <li class="nav-item">
                <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-book"></i>
                    <p>LAPORAN</p>
                </a>
            </li>  
           
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>