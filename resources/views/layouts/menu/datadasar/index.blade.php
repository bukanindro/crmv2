@extends('layouts.dashboard')

@section('content')
<div class="card">
    <div class="card-header">
        Data Dasar
    </div>
    {{-- <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif --}}
        <div class="card-primary card-outline card-outline-tabs ">
            <div class="card-header p-0 border-bottom-0">
              <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" id="produk-tab" data-toggle="pill" href="#produk" role="tab" aria-controls="produk" aria-selected="true">Produk</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="status-tab" data-toggle="pill" href="#status" role="tab" aria-controls="status" aria-selected="false">Status</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="dokumen-tab" data-toggle="pill" href="#dokumen" role="tab" aria-controls="dokumen" aria-selected="false">Status Dokumen</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" id="kegiatan-tab" data-toggle="pill" href="#kegiatan" role="tab" aria-controls="kegiatan" aria-selected="false">Kegiatan</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="kontak-tab" data-toggle="pill" href="#kontak" role="tab" aria-controls="kontak" aria-selected="false">Jenis Kontak</a>
                </li>
              </ul>
            </div>
            <div class="card-body">
              @if(session('sukses'))
              <div class="alert alert-success" role="alert">
                {{session('sukses')}}
              </div>
              @endif
              <div class="tab-content" id="produk-tabContent">

            {{-- Panel Produk --}}
                <div class="tab-pane fade active show" id="produk" role="tabpanel" aria-labelledby="produk-tab">
                    <div class="row">
                        <div class="col-12">
                            <button type="button" class="btn bg-gradient-info" data-toggle="modal" data-target="#modal-default">Tambah</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-body table-responsive p-0">
                              <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th>Kode</th>
                                    <th>Jenis Produk</th>
                                    <th>Aksi</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach ($data_produk ?? '' as $produk)
                                  <tr>
                                    <td>{{$produk->kode}}</td>
                                    <td>{{$produk->nama_produk}}</td>
                                    <td>
                                      <a href="/produk/{{$produk->kode}}/edit" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                      <a href="/produk/{{$produk->kode}}/delete" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>                                   
                                  @endforeach
                                </tbody>
                              </table>
                            </div>
                            <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                        </div>
                      </div>
                    </div>
                {{-- Modal Produk --}}
                  <div class="modal fade" id="modal-default" style="display: none;" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Tambah Data</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">×</span>
                            </button>
                          </div>
                          <div class="modal-body">
                            <form action="/produk/create" method="POST">
                              {{csrf_field()}}
                              <div class="form-group row">
                                  <label for="prod_id" class="col-sm-2 col-form-label">Kode</label>
                                  <div class="col-sm-10">
                                    <input name= "kode" type="text" class="form-control">
                                  </div>
                                </div>
                                <div class="form-group row">
                                  <label for="prod_jenis" class="col-sm-2 col-form-label">Jenis</label>
                                  <div class="col-sm-10">
                                    <input name="nama_produk" type="text" class="form-control">
                                  </div>
                                </div>
                              <div class="modal-footer justify-content-between">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn bg-gradient-info">Simpan</button>
                              </div>
                          </form>
                          </div>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                <div class="tab-pane fade" id="status" role="tabpanel" aria-labelledby="status-tab">
                    <div class="row">
                        <div class="col-12">
                            <button type="button" class="btn bg-gradient-info" data-toggle="modal" data-target="#modal-default">Tambah</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-body table-responsive p-0">
                              <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>183</td>
                                    <td>John Doe</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>219</td>
                                    <td>Alexander Pierce</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>657</td>
                                    <td>Bob Doe</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>175</td>
                                    <td>Mike Doe</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                            <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                        </div>
                      </div>
                    </div>
                <div class="tab-pane fade" id="dokumen" role="tabpanel" aria-labelledby="dokumen-tab">
                    <div class="row">
                        <div class="col-12">
                            <button type="button" class="btn bg-gradient-info" data-toggle="modal" data-target="#modal-default">Tambah</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-body table-responsive p-0">
                              <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>183</td>
                                    <td>John Doe</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>219</td>
                                    <td>Alexander Pierce</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>657</td>
                                    <td>Bob Doe</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>175</td>
                                    <td>Mike Doe</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                            <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                        </div>
                      </div>
                    </div>
                <div class="tab-pane fade" id="kegiatan" role="tabpanel" aria-labelledby="kegiatan-tab">
                    <div class="row">
                        <div class="col-12">
                            <button type="button" class="btn bg-gradient-info" data-toggle="modal" data-target="#modal-default">Tambah</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-body table-responsive p-0">
                              <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>183</td>
                                    <td>John Doe</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>219</td>
                                    <td>Alexander Pierce</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>657</td>
                                    <td>Bob Doe</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>175</td>
                                    <td>Mike Doe</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                            <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                        </div>
                      </div>
                    </div>
                <div class="tab-pane fade" id="kontak" role="tabpanel" aria-labelledby="kontak-tab">
                    <div class="row">
                        <div class="col-12">
                            <button type="button" class="btn bg-gradient-info" data-toggle="modal" data-target="#modal-default">Tambah</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                          <div class="card">
                            <div class="card-body table-responsive p-0">
                              <table class="table table-hover">
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>Jenis Kontak</th>
                                    <th>Aksi</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <td>183</td>
                                    <td>John Doe</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>219</td>
                                    <td>Alexander Pierce</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>657</td>
                                    <td>Bob Doe</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>175</td>
                                    <td>Mike Doe</td>
                                    <td>
                                        <a href="" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                                        <a href="" class="btn btn-danger btn-sm fas fa-trash"></a>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                            <!-- /.card-body -->
                          </div>
                          <!-- /.card -->
                        </div>
                      </div>
                </div>
              </div>
            </div>
            <!-- /.card -->
            
          </div>
        
    {{-- </div> --}}
</div>
@endsection
