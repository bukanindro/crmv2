@extends('layouts.dashboard')

@section('content')
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Instansi</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form action="{{ route('instansi.store') }}" method="POST">
              @csrf
              <form class="form-horizontal">
                <div class="card-body">
                  <div class="form-group row">
                    <label for="kode" class="col-sm-2 col-form-label">Kode Instansi</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="kode" name="kode" placeholder="Kode Instansi">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="nama_instansi" class="col-sm-2 col-form-label">Nama Instansi</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="nama_instansi" placeholder="Nama Instansi" name="nama_instansi">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="jenis_instansi" class="col-sm-2 col-form-label">Jenis Instansi</label>
                    <div class="col-sm-10">
                      <div class="form-group">
                        <select name="id" class="form-control">
                          <option value="">- Pilih -</option>
                          @foreach ($jenis_instansi as $key => $val)
                            <option value="{{ $key }}">{{$val}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="provinsi" class="col-sm-2 col-form-label">Provinsi</label>
                    <div class="col-sm-10">
                      <div class="form-group">
                        <select name="kode_prov" class="form-control">
                          <option value="">- Pilih -</option>
                          @foreach ($provinsi as $key => $val)
                            <option value="{{ $key }}">{{$val}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="kabupaten" class="col-sm-2 col-form-label">Kabupaten</label>
                    <div class="col-sm-10">
                      <div class="form-group">
                        <select name="kode_kab" class="form-control">
                          <option value="">- Pilih -</option>
                          @foreach ($kabupaten as $key => $val)
                            <option value="{{ $key }}">{{$val}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Simpan</button>
                  <button type="submit" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </form>      
        </div>                    
@endsection