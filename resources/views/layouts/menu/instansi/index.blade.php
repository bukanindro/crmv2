@extends('layouts.dashboard')

@section('content')
@include('layouts.partials.flash')
<div class="card">
    <div class="card-header">
        <h3>Instansi</h3>
    </div>
    <div class="card-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="card-header">
            <div class="card-title">
              <a href="/instansi/create" class="btn btn-info bg-gradient-info">Tambah</a>
            </div>
                <div class="card-tools">
                    <div class="input-group input-group-sm" style="width: 150px;">
                        <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                        <div class="input-group-append">
                            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
        </div>
        <div class="card-body table-responsive p-0">
            <table class="table table-hover">
                <thead>
                    <tr>
                    <th>Kode</th>
                    <th>Nama Instansi</th>
                    <th>Kabupaten</th>
                    <th>Jenis</th>
                    <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data_instansi ?? '' as $instansi)
                    <tr>
                    <td>{{$instansi->kode}}</td>
                    <td>{{$instansi->nama_instansi}}</td>
                    <td>{{$instansi->kode_kab}}</td>
                    <td>{{$instansi->jenis_instansi}}</td>
                    <td>
                        <a href="/instansi/{{$instansi->id}}/edit" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                        <a href="/instansi/{{$instansi->id}}/delete" class="btn btn-danger btn-sm fas fa-trash"></a>
                        </td>
                    </tr>    
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
