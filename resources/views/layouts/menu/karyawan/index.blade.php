@extends('layouts.dashboard')

@section('content')
<div class="card">
    <div class="card-header">
        <h3>Data Karyawan</h3>
    </div>
    <div class="card-body">
            <div class="card">
                @if(session('sukses'))
                  <div class="alert alert-success" role="alert">
                    {{session('sukses')}}
                  </div>
                @endif
                <div class="card-header">
                    <div class="card-title">
                      <a href="/karyawan/create" class="btn btn-info bg-gradient-info">Tambah</a>
                    </div>
                        <div class="card-tools">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
  
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                  <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>No Telepon</th>
                        <th>No Rek</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($data_karyawan ?? '' as $karyawan)
                      <tr>
                        <td>{{$karyawan->id}}</td>
                        <td>{{$karyawan->nama}}</td>
                        <td>{{$karyawan->telp_1}}</td>
                        <td>{{$karyawan->no_rek}}</td>
                        <td>
                            <a href="/karyawan/{{$karyawan->id}}/edit" class="btn btn-warning btn-sm fas fa-pencil-alt"></a>
                            <a href="/karyawan/{{$karyawan->id}}/delete" class="btn btn-danger btn-sm fas fa-trash"></a>
                          </td>
                      </tr>    
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
            </div>        
    </div>
</div>
@endsection
