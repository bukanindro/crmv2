@extends('layouts.dashboard')

@section('content')
        <div class="card card-info">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Karyawan</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <form action="{{ route('karyawan.store') }}" method="POST">
              @csrf
              <form class="form-horizontal">
                <div class="card-body">
                  <div class="form-group row">
                    <label for="nama" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="tlp1" class="col-sm-2 col-form-label">No. Telp 1</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="tlp1" placeholder="Nomor Telepon" name="telp_1">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="tlp2" class="col-sm-2 col-form-label">No. Telp 2</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="tlp2" placeholder="Nomor Telepon" name="telp_2">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="no_rek" class="col-sm-2 col-form-label">No. Rek</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="no_rek" placeholder="Nomor Rekening" name="no_rek">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="nik" class="col-sm-2 col-form-label">NIK</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="nik" placeholder="NIK" name="nik">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="npwp" class="col-sm-2 col-form-label">NPWP</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="npwp" placeholder="NPWP" name="npwp">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="bpjs_kes" class="col-sm-2 col-form-label">BPJS Kesehatan</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="bpjs_kes" placeholder="BPJS Kesehatan" name="bpjs_kes">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="bpjs_tk" class="col-sm-2 col-form-label">BPJS Ketenagakerjaan</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="bpjs_tk" placeholder="BPJS Ketenagakerjaan" name="bpjs_tk">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Mulai Kerja</label>
                    <div class="col-sm-10">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                        </div>
                        <input type="text" class="form-control" data-inputmask-alias="datemask" data-inputmask-inputformat="dd/mm/yyyy" im-insert="false" name="tgl_kerja" placeholder="yyyy/mm/dd">
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="driver" class="col-sm-2 col-form-label">Driver</label>
                    <div class="col-sm-10">
                      <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="customRadio1" name="driver" value="1">
                        <label for="customRadio1" class="custom-control-label">Ya!</label>
                      </div>
                      <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="customRadio2" name="driver" checked="" value="0">
                        <label for="customRadio2" class="custom-control-label">Tidak!</label>
                      </div>
                    </div>  
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-info">Simpan</button>
                  <button type="submit" class="btn btn-default float-right">Cancel</button>
                </div>
                <!-- /.card-footer -->
              </form>
            </form>      
        </div>                    
@endsection