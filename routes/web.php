<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/datadasar', 'Datadasar\ProdukController@index')->name('index');
// Route::get('/datadasar', 'Datadasar\StatusController@index')->name('index');

Route::post('produk/create', 'Datadasar\ProdukController@create');
Route::get('produk/{kode}/edit', 'Datadasar\ProdukController@edit');
Route::get('produk/{kode}/delete', 'Datadasar\ProdukController@delete');


Route::get('karyawan', 'KaryawanController@index')->name('karyawan.index');
Route::get('karyawan/create', 'KaryawanController@create')->name('karyawan.create');
Route::post('karyawan', 'KaryawanController@store')->name('karyawan.store');
Route::get('karyawan/{id}/delete', 'KaryawanController@delete');

Route::get('instansi', 'InstansiController@index')->name('instansi.index');  
Route::get('instansi/create', 'InstansiController@create')->name('instansi.create');
Route::post('instansi', 'InstansiController@store')->name('instansi.store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

  
