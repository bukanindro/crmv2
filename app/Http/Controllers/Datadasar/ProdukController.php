<?php

namespace App\Http\Controllers\Datadasar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class ProdukController extends Controller
{
    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function index()
    {
        $data_produk = Product::all();
        return view('layouts.menu.datadasar.index',['data_produk' => $data_produk]);
    }

    public function create(Request $request)
    {
        Product::create($request->all());
        return redirect('/datadasar')->with('sukses','Data berhasil disimpan!');
    }

    public function edit($kode)
    {
        $data_produk = Product::find($kode);
        return view('/produk/edit');
    }

    public function delete($kode)
    {
        $data_produk = $this->product->where('kode', $kode)->first();

        if ($data_produk != null) {
            $data_produk->delete();
            return redirect('/datadasar')->with('sukses','Data berhasil dihapus!');
        }
        return redirect('/datadasar')->with('sukses','Data gagal dihapus!');
    }
}
