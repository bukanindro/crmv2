<?php

namespace App\Http\Controllers\Datadasar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    protected $status;

    public function __contstruct(Status $status)
    {
        $this->status = $status;
    }

    public function index()
    {
        $data_status = \App\Models\Status::all();
        return view('layouts.menu.datadasar.index');
    }
}
