<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Karyawan;

class KaryawanController extends Controller
{
    protected $karyawan;

    public function __construct(Karyawan $karyawan)
    {
        $this->karyawan = $karyawan;
    }

    public function index()
    {
        $data_karyawan = Karyawan::all();
        return view('layouts.menu.karyawan.index',['data_karyawan' => $data_karyawan]);
    }

    public function create()
    {
        return view('layouts.menu.karyawan.create')->with('sukses','Data Berhasil Disimpan'); 
    }

    public function store()
    {
        // save
        // request()->merge(['driver' => request()->has('driver') ? 1 : 0]);
        $this->karyawan->create(request()->all());

        return redirect()->back();
    }

    public function delete($id)
    {
        $karyawan = Karyawan::FindOrFail($id);

        $karyawan->delete();

        return redirect()->route('karyawan.index')->with('status','Data berhasil dihapus!');
    }

}
