<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\Instansi;
use Illuminate\Support\Facades\DB;

class InstansiController extends Controller
{
    protected $instansi;

    public function __construct(Instansi $instansi)
    {
        $this->instansi = $instansi;
    }

    public function index()
    {
        $data_instansi = Instansi::all();
        return view('layouts.menu.instansi.index',['data_instansi' => $data_instansi]);
    }

    public function create()
    {
        $provinsi = DB::table('provinsi')->pluck('nama', 'kode');
        $kabupaten = DB::table('kabupaten')->pluck('nama', 'kode');
        $jenis_instansi = DB::table('jenis_instansi')->pluck('jenis_instansi', 'id');

        return view('layouts.menu.instansi.create')->with([
            'provinsi' => $provinsi,
            'kabupaten' => $kabupaten,
            'jenis_instansi' => $jenis_instansi
        ]);
    }

    public function store()
    {
        $this->instansi->create(request()->all());

        return redirect()->back()->with('sukses','Data Berhasil Disimpan');
    }
}
