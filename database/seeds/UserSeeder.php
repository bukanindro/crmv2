<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert([
            [
                'name' => 'Administrator',
                'email' => 'admin@me.com',
                'password' => Hash::make('admin123')
            ],

            [
                'name' => 'User',
                'email' => 'user@me.com',
                'password' => Hash::make('user123')
            ],

            [
                'name' => 'Super Administrator',
                'email' => 'superadmin@me.com',
                'password' => Hash::make('dewaadmin')
            ],
        ]);
    }
}
