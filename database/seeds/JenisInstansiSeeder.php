<?php

use Illuminate\Database\Seeder;

class JenisInstansiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_instansi')->truncate();
        DB::table('jenis_instansi')->insert([
            [
                'id' => '01',
                'jenis_instansi' => 'Dinas'
            ],
            [
                'id' => '02',
                'jenis_instansi' => 'Puskesmas'
            ],
            [
                'id' => '03',
                'jenis_instansi' => 'Klinik'
            ],
            [
                'id' => '04',
                'jenis_instansi' => 'Lain-lain'
            ],
        ]);
    }
}
