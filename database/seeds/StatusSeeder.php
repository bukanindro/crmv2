<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->truncate();
        DB::table('status')->insert([
            [
                'kode' => '01',
                'status' => 'Sudah'
            ],

            [
                'kode' => '02',
                'status' => 'Belum'
            ],

            [
                'kode' => '03',
                'status' => 'Terjadwal'
            ],

            [
                'kode' => '04',
                'status' => 'Reschedule'
            ]
        ]);
    }
}
