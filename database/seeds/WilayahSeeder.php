<?php

use Illuminate\Database\Seeder;

class WilayahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('kecamatan')->truncate();
        DB::table('kabupaten')->truncate();
        DB::table('provinsi')->truncate();
        
        Eloquent::unguard();

        $path = 'database/provinsi.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Provinsi table seeded!');

        $path = 'database/kabupaten.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Kabupaten table seeded!');

        $path = 'database/kecamatan.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Kecamatan table seeded!');

    }
}
