<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKaryawanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karyawan', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('nama', 100)->nullable();
            $table->char('telp_1', 15)->nullable();
            $table->char('telp_2', 15)->nullable();
            $table->char('no_rek', 25)->nullable();
            $table->date('tgl_kerja')->nullable();
            $table->char('nik', 16)->nullable();
            $table->char('npwp', 25)->nullable();
            $table->char('bpjs_kes', 13)->nullable();
            $table->char('bpjs_tk', 16)->nullable();
            $table->char('driver', 2)->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kayrawan');
    }
}
